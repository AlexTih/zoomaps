package com.hfad.zoo.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import com.hfad.zoo.R;
import com.hfad.zoo.config.AnimalsConfig;
import com.hfad.zoo.models.AnimalsModel;
import com.squareup.picasso.Picasso;
import java.io.IOException;


public class DetailsActivity extends AppCompatActivity {
    ImageView imageView;
    TextView title;
    TextView textDescription;
    AnimalsModel animalsModel;

    public void init() throws IOException {
        Intent intent = getIntent();
        animalsModel = (AnimalsModel) intent.getSerializableExtra(AnimalsConfig.KEY_INTENT_ANIMALS);
        imageView = (ImageView) findViewById(R.id.image_url);
        title = (TextView) findViewById(R.id.title_animal);
        textDescription = (TextView) findViewById(R.id.text_description);
        title.setText(animalsModel.getName());
        textDescription.setText(animalsModel.getDescription());
        Picasso.with(this).load(animalsModel.getUrl()).placeholder(R.drawable.normal).error(R.drawable.crash_android).into(imageView);

    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        try {
            init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
