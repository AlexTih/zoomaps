package com.hfad.zoo.ui;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.ArrayMap;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hfad.zoo.R;
import com.hfad.zoo.config.AnimalsConfig;
import com.hfad.zoo.models.AnimalsModel;
import com.hfad.zoo.providers.AnimalsModelProvider;
import java.io.IOException;
import java.util.ArrayList;


public class MainActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, ActivityCompat.OnRequestPermissionsResultCallback, TextView.OnEditorActionListener {
    private static final int MY_LOCATION_REQUEST_CODE = 1;
    SupportMapFragment mapFragment;
    GoogleMap map;
    Marker marker;
    Intent intent;
    ArrayList<AnimalsModel> animalsModels;
    EditText editSearch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapView);
        mapFragment.getMapAsync(this);
        editSearch = (EditText) findViewById(R.id.editSearch);
        editSearch.setOnEditorActionListener(this);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == MY_LOCATION_REQUEST_CODE && grantResults.length == 2) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                map.setMyLocationEnabled(true);
            }
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, MY_LOCATION_REQUEST_CODE);
        }

        map.getUiSettings().setMyLocationButtonEnabled(true);
        map.getUiSettings().setMapToolbarEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);

        ArrayList<Marker> markers = new ArrayList<>();
        ArrayMap<Integer, Integer> arrayMaps = new ArrayMap<>();
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList.add(R.drawable.lion);
        arrayList.add(R.drawable.elephant);
        arrayList.add(R.drawable.wolf);
        arrayList.add(R.drawable.giraffe);
        arrayList.add(R.drawable.hippopotamus);

        try {
            animalsModels = AnimalsModelProvider.providerAnimalModels(this);
            for (int i = 0; i < animalsModels.size(); i++) {
                arrayMaps.put(animalsModels.get(i).getType(), arrayList.get(i));
                LatLng animalsLocation = new LatLng(animalsModels.get(i).getLat(), animalsModels.get(i).getLng());
                marker = map.addMarker(new MarkerOptions().position(animalsLocation).title(animalsModels.get(i).getName()).icon(BitmapDescriptorFactory.fromResource(arrayMaps.get(i))));
                marker.setTag(animalsModels.get(i));
                markers.add(marker);
            }
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            for (Marker marker : markers) {
                builder.include(marker.getPosition());
            }
            LatLngBounds bounds = builder.build();
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, 2);
            map.animateCamera(cu);
        } catch (IOException e) {
            e.printStackTrace();
        }
        map.setOnMarkerClickListener(this);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        AnimalsModel animalsModels = (AnimalsModel) marker.getTag();
        intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(AnimalsConfig.KEY_INTENT_ANIMALS, animalsModels);
        startActivity(intent);
        return false;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        ArrayList<AnimalsModel> a = new ArrayList<>();
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            for(int i = 0; i < animalsModels.size(); i++) {
                a.add(animalsModels.get(i));
            }
            for (int i = 0; i < a.size(); i++) {
                if (editSearch.getText().toString().equals(a.get(i).getName())) {
                    map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(a.get(i).getLat(), a.get(i).getLng())));
                }
            }
        }
        return false;
    }
}
